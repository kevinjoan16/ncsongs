package models;

import akka.japi.Pair;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 * Created by kecc on 13/07/17.
 */
@Singleton
public class ArtistaRepository extends AbstractRepository<Artista> {

    @Inject
    public ArtistaRepository(JPAApi jpaApi, DatabaseExecutionContext ec){
        super(jpaApi, ec);
    }

    @Override
    public CompletionStage<Optional<Artista>> get(Artista data) {
        return CompletableFuture.supplyAsync(() -> wrap(em -> lookup(em, data.getId())));
    }

    @Override
    protected Stream<Artista> list(EntityManager em) {
        return em.createQuery("select a from Artista a", Artista.class).getResultList().stream();
    }

    public CompletionStage<Pair<Artista, Stream<Cancion>>> listCanciones(Integer artistaId){
        return CompletableFuture.supplyAsync(() -> wrap(em -> listCanciones(em, artistaId)));
    }

    public Pair<Artista, Stream<Cancion>> listCanciones(EntityManager em, Integer id){
        Optional<Artista> o = Optional.ofNullable(em.find(Artista.class, id));
        if(o.isPresent()){
            return new Pair<>(o.get(), new ArrayList<Cancion>(o.get().getCanciones()).stream());
        }else{
            return new Pair<>(null, null);
        }
    }

    /**
     * Retorna el artista buscado por id
     * @param em Entity manager
     * @param id Id del artista a buscar
     * @return Opcional del artista a buscado
     */
    private Optional<Artista> lookup(EntityManager em, Integer id){
        return Optional.ofNullable(em.find(Artista.class, id));
    }

    @Override
    public Optional<Artista> modify(EntityManager em, Artista newData){
        Artista artista = em.find(Artista.class, newData.getId());
        if(artista != null){
            artista.setNombre(newData.getNombre());
        }
        return Optional.ofNullable(artista);
    }

    @Override
    protected Artista delete(EntityManager em, Artista delete) {
        Artista a = em.find(Artista.class, delete.getId());
        if(a.getContrasena().equals(delete.getContrasena())){
            em.remove(a);
            return a;
        }
        return null;
    }
}
