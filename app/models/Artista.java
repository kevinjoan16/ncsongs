package models;

import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.List;

/**
 * Created by kecc on 13/07/17.
 * Entidad que modela un Artista
 */
@Entity
@Table(name = "artista")
public class Artista {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String usuario;

    @Column(nullable = false)
    private String contrasena;

    @Constraints.Required
    @Column(nullable = false)
    private String nombre;

    @OneToMany(mappedBy = "artista")
    private List<Cancion> canciones;

    /**
     * Ruta de la imagen del perfil (opcional)
     */
    @Column(name = "ruta_imagen")
    private String rutaImagen;

    public Artista() {
    }

    public Artista(String email, String usuario, String contrasena, String nombre) {
        this.email = email;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    public List<Cancion> getCanciones() {
        return canciones;
    }

    public void setCanciones(List<Cancion> canciones) {
        this.canciones = canciones;
    }
}
