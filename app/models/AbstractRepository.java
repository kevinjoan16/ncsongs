package models;

import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created by kecc on 13/07/17.
 * Clase abstracta con las operaciones basicas para un repositorio
 */
public abstract class AbstractRepository<T> {

    protected JPAApi jpaApi;
    protected DatabaseExecutionContext ec;

    @Inject
    public AbstractRepository(JPAApi jpaApi, DatabaseExecutionContext ec){
        this.jpaApi = jpaApi;
        this.ec = ec;
    }

    /**
     * Stream de objetos de entidad T presentes en la base de datos
     * @return Retorna una fase que devuelve el stream de entidades T
     */
    public CompletionStage<Stream<T>> list(){
        return CompletableFuture.supplyAsync(() -> wrap(em -> list(em)), ec);
    }

    /**
     * Obtiene una entidad T segun su id
     * @param data Objeto T con id definido a buscar
     * @return Fase que devuelve un opcional de la entidad T
     */
    public abstract CompletionStage<Optional<T>> get(T data);

    /**
     * Crea un objeto T
     * @param data Objeto T a crear
     * @return Fase que devuelve la entidad T ej caso de ser creado
     */
    public CompletionStage<T> create(T data){
        return CompletableFuture.supplyAsync(() -> wrap(em -> insert(em, data)), ec);
    }

    /**
     * Actualiza la informacion de una entidad T
     * @param newData Entidad T con informacon nueva
     * @return Fase que devuelve un opcional de la entidad T con la informacion nueva
     */
    public CompletionStage<Optional<T>> update(T newData){
        return CompletableFuture.supplyAsync(() -> wrap(em -> modify(em, newData)));
    }

    /***
     * Metodo que encapsula una funcion para ser ejecutada en una transaccion de la base de datos
     * @param function Funcion a ser ejecutada en la transaccion
     * @param <T> Tipo de retorno de la funcion
     * @return Retorna el objeto a ser retornado por la funcion encapsulada
     */
    protected <T> T wrap(Function<EntityManager, T> function){
        return jpaApi.withTransaction(function);
    }

    /**
     * Inserta una entidad en la base de datos
     * @param em Entity manager
     * @param data informacion a ser guardada en la base de datos
     * @return Entidad guardada en la base de datos
     */
    protected T insert(EntityManager em, T data){
        em.persist(data);
        return data;
    }

    /**
     * Borra una entidad de la base de datos
     * @param delete Entidad a ser borrada
     * @return Fase que devuelve la entidad eliminada
     */
    public CompletionStage<T> delete(T delete){
        return CompletableFuture.supplyAsync(() -> wrap(em -> delete(em, delete)));
    }

    /**
     * Borra la entidad en la base datos
     * @param em Entity manager
     * @param delete Entidad a ser borrada
     * @return Entidad borrada
     */
    protected abstract T delete(EntityManager em, T delete);

    /**
     * Retorna el stream de entidades presentes en la base de datos
     * @param em Entity manager
     * @return Stream de entidades presentes en la base datos
     */
    protected abstract Stream<T> list(EntityManager em);

    /**
     * Modifica una entidad en la base de datos
     * @param em Entity manager
     * @param newData Entidad con la informacion nueva
     * @return Opcional de la entidad actualizada
     */
    protected abstract Optional<T> modify(EntityManager em, T newData);
}
