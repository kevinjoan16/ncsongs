package controllers;

import com.cloudinary.utils.ObjectUtils;
import com.google.common.io.Files;
import models.Artista;
import models.ArtistaRepository;
import models.Cancion;
import models.CancionRepository;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import com.cloudinary.*;
/**
 * Created by kecc on 15/07/17.
 */
public class CancionController extends Controller {

    private final FormFactory formFactory;
    private final ArtistaRepository artistaRepository;
    private final CancionRepository cancionRepository;
    private final HttpExecutionContext executionContext;
    private Cloudinary cloudinary;

    /**
     * Constructor del controlador, crea una instancia del controlador mediante la inyeccion de dependencias
     * @param formFactory Fabrica de formularios
     * @param artistaRepository Repositorio de artistas
     * @param cancionRepository Repositorio de canciones
     * @param executionContext Contexto de ejecucion HTTP
     */
    @Inject
    public CancionController(FormFactory formFactory, ArtistaRepository artistaRepository, CancionRepository cancionRepository, HttpExecutionContext executionContext) {
        this.formFactory = formFactory;
        this.artistaRepository = artistaRepository;
        this.cancionRepository = cancionRepository;
        this.executionContext = executionContext;
        //Crea una instancia de cloudinary para acceder a su almacenamiento
        this.cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "ddoag8jve",
                "api_key", "569318288652417",
                "api_secret", "72CmJ4EUlINFl5apHV0LlKXEJHA"));
    }

    /**
     * Retorna la pagina con el formulario para agregar una cancion
     * @return Pagina con el formulario para agregar una cancion
     */
    public CompletionStage<Result> pageAddCancion(){
        return artistaRepository.list().thenApplyAsync(artistaStream -> {
            return ok(views.html.cancionNueva.render(formFactory.form(Cancion.class), artistaStream.collect(Collectors.toList())));
        }, executionContext.current());
    }

    /**
     * Agrega una cancion
     * @return Pagina de inicio en caso de exito, en caso de error muestra un mensaje indicandolo
     */
    public CompletionStage<Result> addCancion(){
        DynamicForm data = formFactory.form().bindFromRequest();
        Artista artista = new Artista();
        //Si no esta el id del artista se asume nulo
        if(data.get("artista_id") != null && !data.get("artista_id").equals("")){
            artista.setId(Integer.parseInt(data.get("artista_id")));
        }else {
            artista = null;
        }
        //Se crea cancion con el nombre y artista dado
        Cancion cancion = new Cancion();
        cancion.setNombre(data.get("nombre"));
        cancion.setArtista(artista);
        return cancionRepository.create(cancion).thenApplyAsync(cancionCreated -> {
            //Al crearse la cancion se procede a subir el archivo de la misma
            //Se obtiene el archivo enviado por la peticion
            Http.MultipartFormData<File> body = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart<File> song = body.getFile("file");
            if(song != null){
                File f = song.getFile();
                try {
                    //Se guarda el archivo y se obtiene la ruta a el
                    String filePath = saveFile(f);
                    //Se asigna la ruta a la cancion y se guarda en la base de datos
                    cancion.setRutaArchivo(filePath);
                    cancionRepository.updateRutaArchivo(cancionCreated);
                    return redirect(routes.HomeController.index());
                }catch (Exception e){
                    //En caso de error se borra la cancion creada
                    cancionRepository.delete(cancionCreated);
                    Logger.error(e.getMessage());
                    return internalServerError("Error uploading the file :(");
                }
            }else{
                //En caso de error se borra la cancion creada
                cancionRepository.delete(cancionCreated);
                return internalServerError("Error uploading the file :(");
            }
        }, executionContext.current());

    }

    /**
     * Borra la cancion dado su id
     * @param id Id de la cancion
     * @return Pagina de inicio en caso de exito sino
     */
    public CompletionStage<Result> delete(Integer id){
        Cancion c = new Cancion();
        c.setId(id);
        return cancionRepository.delete(c).thenApplyAsync(removed -> {
            if(removed != null){
                return redirect(routes.HomeController.index());
            }else{
                return internalServerError("Ocurrio un error al intentar borrar la cancion :(");
            }
        });
    }

    /**
     * Guarda el archivo en cloudinary
     * @param source Archivo de origen
     * @return URL del archivo
     * @throws Exception
     */
    private String saveFile(File source) throws Exception{
        Map uploadResult = cloudinary.uploader().upload(source, ObjectUtils.asMap(
                "resource_type", "auto"
        ));
        return uploadResult.get("url").toString();
    }
}
