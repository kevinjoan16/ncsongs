package controllers;

import models.Artista;
import models.ArtistaRepository;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 * Created by kecc on 13/07/17.
 * Controlador que contiene las acciones para agregar, mostrar, modificar y borrar un artisa
 */
public class ArtistaController extends Controller {

    private final FormFactory formFactory;
    private final ArtistaRepository artistaRepository;
    private final HttpExecutionContext executionContext;

    /**
     * Constructor del controlador, se instancia mediante inyeccion de dependencias
     * @param formFactory Fabrica de formularios
     * @param artistaRepository Repositorio de formularios
     * @param executionContext Contexto de ejecución
     */
    @Inject
    public ArtistaController(FormFactory formFactory, ArtistaRepository artistaRepository, HttpExecutionContext executionContext) {
        this.formFactory = formFactory;
        this.artistaRepository = artistaRepository;
        this.executionContext = executionContext;
    }

    /**
     * Agrega un artista enviado en el cuerpo de la petición
     * @return Resultado, redirige a index si pudo agregar sino muestra de nuevo el formulario de registro
     */
    public CompletionStage<Result> addArtista(){
        Form<Artista> data = formFactory.form(Artista.class).bindFromRequest();
        if(data.hasErrors()){
            return CompletableFuture.supplyAsync(() -> badRequest(views.html.signup.render(data)), executionContext.current());
        }else{
            Artista artista = data.get();
            return artistaRepository.create(artista).thenApplyAsync(a -> {
                return redirect(routes.ArtistaController.show(artista.getId()));
            }, executionContext.current()).exceptionally(throwable -> redirect(routes.HomeController.signup()));
        }
    }

    /**
     * Borra un artista dado su id y contraseña
     * @param id Id del artista
     * @return Redirige al index en caso de poder eliminar al artista
     */
    public CompletionStage<Result> delete(Integer id){
        DynamicForm data = formFactory.form().bindFromRequest();
        Artista a = new Artista();
        a.setId(id);
        a.setContrasena(data.get("contrasena"));
        return artistaRepository.delete(a).thenApplyAsync((removed) -> {
            return redirect(routes.HomeController.index());
        });
    }

    /**
     * Muestra una pagina con los datos del artista y sus canciones
     * @param id Id del artisa
     * @return Pagina con los datos del artista y sus canciones
     */
    public CompletionStage<Result> show(Integer id){
        Artista a = new Artista();
        a.setId(id);
        return artistaRepository.listCanciones(id).thenApplyAsync(pair -> {
            if(pair.first() != null){
                return ok(views.html.artistaHome.render(pair.first(), pair.second().collect(Collectors.toList())));
            }else{
                return redirect(routes.HomeController.index());
            }
        }, executionContext.current());
    }

    /**
     * Modifica el nombre de un artista dado su id y el nuevo nombre en la peticion
     * @param id id del artista
     * @return Pagina actualizada de los datos del artista
     */
    public CompletionStage<Result> modify(Integer id){
        DynamicForm data = formFactory.form().bindFromRequest();
        Artista a = new Artista();
        a.setId(id);
        a.setNombre(data.get("nombre"));
        return artistaRepository.update(a).thenApplyAsync(artista -> {
            return redirect(routes.ArtistaController.show(id));
        });
    }
}
