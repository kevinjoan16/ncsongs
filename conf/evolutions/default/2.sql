# --- Se agregan unique constraint a tabla artista
# --- !Ups
ALTER TABLE artista ADD CONSTRAINT uq_artista_usuario UNIQUE(usuario);
ALTER TABLE artista ADD CONSTRAINT uq_artista_email UNIQUE(email);

# --- !Downs
ALTER TABLE artista DROP CONSTRAINT uq_artista_usuario;
ALTER TABLE artista DROP CONSTRAINT uq_artista_email;