# Al borrar un artista se define la llave foranea a null

# --- !Ups

ALTER TABLE cancion DROP CONSTRAINT fk_cancion_artista, ADD CONSTRAINT fk_cancion_artista FOREIGN KEY (artista_id) REFERENCES artista(id) ON DELETE SET NULL;

# --- !Downs

ALTER TABLE cancion DROP CONSTRAINT fk_cancion_artista, ADD CONSTRAINT fk_cancion_artista FOREIGN KEY (artista_id) REFERENCES artista(id);